import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by kirif on 03.07.2017.
 */
class Calculator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        for (; ;) {
            try {
                System.out.println("First number");
                double numberOne = input.nextDouble();
                System.out.println("Please enter operation");
                String action = input.next();
                System.out.println("Second number");
                double numberTwo = input.nextDouble();
                if (numberTwo == 0){
                    System.out.println("Don't use 0 as divider");
                } else {
                    System.out.println("Your answer is: " + calculation(numberOne, numberTwo, action));
                }
            }
            catch (InputMismatchException e) {
                System.out.println("Enter the digital");
                break;
            }
            catch (Exception e) {
                System.out.println("Something goes wrong");
                break;
            }
            System.out.println("Do you want to continue work with calculator(Choose yes/no)?");
            String answer = input.next();
            if (answer.equals("yes")) {
                continue;
            } else {
                System.out.println("Thanks for calculation");
                break;
            }
        }
    }

    public static double calculation(double numberOne, double numberTwo, String action) {
        switch(action) {
            case "+":
                return numberOne + numberTwo;
            case "-":
                return numberOne - numberTwo;
            case "*":
                return numberOne * numberTwo;
            case "/":
                return numberOne / numberTwo;
            default:
                System.out.println("You must choose one of this: + , - , * , /");
        }
        return 0;
    }
}